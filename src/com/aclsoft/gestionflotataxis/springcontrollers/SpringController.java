package com.aclsoft.gestionflotataxis.springcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.aclsoft.gestionflotataxis.model.Taxi;
import com.aclsoft.gestionflotataxis.services.ClienteDAO;
import com.aclsoft.gestionflotataxis.services.TaxiDAO;
import com.aclsoft.gestionflotataxis.services.TaxistaDAO;

@Controller
@RequestMapping("/servicios")
public class SpringController {
	
	@Autowired
	private TaxiDAO taxiDAO; //No hace falta setters, lo hace por reflection
	
	@Autowired
	private ClienteDAO clienteDAO; //No hace falta setters, lo hace por reflection
	
	@Autowired
	private TaxistaDAO tacistaDAO; //No hace falta setters, lo hace por reflection

	@RequestMapping(value="/prueba", method = RequestMethod.GET)
	public String xxxxxxxx(){
		System.out.println("ENTRAMOS EN xxxxxxxx");
		return "prueba";
	}
	
	
	@RequestMapping(value="/taxis", method = RequestMethod.GET)
	public @ResponseBody String getTaxis(){
		List<Taxi> taxis = taxiDAO.getAll();
		
		return taxis.toString();
	}
	
	@RequestMapping(value="/taxis/{matricula}", method = RequestMethod.GET)
	public @ResponseBody String loadTaxi(@PathVariable String matricula){
		Taxi taxi = taxiDAO.getByMatricula(matricula);
		
		return taxi.toString();
	}
	
	@RequestMapping(value="/pagina1", method = RequestMethod.GET)
	public ModelAndView pagina1(){
		ModelAndView modelAndView = new ModelAndView("listadoTaxi");
		
		List<Taxi> taxis = taxiDAO.getAll();
		
		//El objeto taxis, la estamos pasando a traves del request
		modelAndView.addObject("listado", taxis);
		System.out.println(taxis.toString());
		return modelAndView;
	}
	

}