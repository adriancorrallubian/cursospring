package com.aclsoft.gestionflotataxis.springcontrollers;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aclsoft.gestionflotataxis.model.Cliente;
import com.aclsoft.gestionflotataxis.model.Taxi;
import com.aclsoft.gestionflotataxis.model.Taxista;
import com.aclsoft.gestionflotataxis.repository.Repository2;
import com.aclsoft.gestionflotataxis.services.TaxiDAO;

@Controller
@RequestMapping("/pruebas")
public class SpringControllerPruebas {
	
@Resource(name="idRepository2")
private Repository2 idRepository2;

@Autowired
private TaxiDAO taxiDAO;

	@RequestMapping(value="/prueba", method = RequestMethod.GET)
	public String xxxxxxxx(){
		System.out.println("ENTRAMOS EN xxxxxxxx");
		return "prueba";
	}
	
	
	@RequestMapping(value="/taxis", method = RequestMethod.GET)
	public @ResponseBody String getTaxis(){
		List<Taxi> taxis = idRepository2.getTaxis();
		
		return taxis.toString();
	}
	
	
	@RequestMapping(value="/clientes", method = RequestMethod.GET)
	public @ResponseBody String getClientes(){
		Map<Integer, Cliente> clientes = idRepository2.getClientes();
		
		return clientes.toString();
	}
	
	
	@RequestMapping(value="/taxistas", method = RequestMethod.GET)
	public @ResponseBody String getTaxistas(){
		Map<Integer, Taxista> taxistas = idRepository2.getTaxistas();
		
		return taxistas.toString();
	}
	
	
	@RequestMapping(value="/crea", method = RequestMethod.GET)
	public @ResponseBody void crea(){
		
		Taxi taxi = new Taxi();
		taxiDAO.save(taxi);
	}
	
//	@RequestMapping(value="/taxis/{matricula}", method = RequestMethod.GET)
//	public @ResponseBody String loadTaxi(@PathVariable String matricula){
//		Taxi taxi = taxiDAO.getByMatricula(matricula);
//		
//		return taxi.toString();
//	}
	
//	@RequestMapping(value="/pagina1", method = RequestMethod.GET)
//	public ModelAndView pagina1(){
//		ModelAndView modelAndView = new ModelAndView("listadoTaxi");
//		
//		List<Taxi> taxis = taxiDAO.getAll();
//		
//		//El objeto taxis, la estamos pasando a traves del request
//		modelAndView.addObject("listado", taxis);
//		System.out.println(taxis.toString());
//		return modelAndView;
//	}
	

}
