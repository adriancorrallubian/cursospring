package com.aclsoft.gestionflotataxis.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.context.support.WebApplicationContextUtils;


/**
 * Servlet implementation class Sv2
 */
@WebServlet("/Sv2")
public class Sv2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	
	ApplicationContext applicationContext = null;
	
			
			
			
	
	
    public Sv2() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		if (applicationContext == null){
			applicationContext = WebApplicationContextUtils.getWebApplicationContext(
					this.getServletContext());
		}
		
		BasicDataSource dataSource =  (BasicDataSource) applicationContext.getBean("dataSource");
				
				
				String user = request.getParameter("user");
			String password = request.getParameter("password");
			
			
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			
			String strCodedPassword = passwordEncoder.encode(password);
			
			
			String strSQL ="INSERT INTO USERS (USERNAME,"+" PASSWORD,"+"ACTIVE,"+" ROL)" + "VALUES (?,?,?,?)";
			
			
			try{
				Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(strSQL);
				ps.setString(1, user);
				ps.setString(2, strCodedPassword);
				ps.setInt(3, 1);
				ps.setString(4, "ROLE_ADMIN");
				ps.execute();
				connection.close();
				
			} catch (SQLException e){
				e.printStackTrace();
			}
				
		
		
		
		
		
		response.sendRedirect("indes.jsp");
		
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
