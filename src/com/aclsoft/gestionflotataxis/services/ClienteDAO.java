package com.aclsoft.gestionflotataxis.services;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.aclsoft.gestionflotataxis.model.Cliente;

public interface ClienteDAO {
	
	public Cliente getById(int id);
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void save(Cliente cliente);
	public List<Cliente> getAll();

}
