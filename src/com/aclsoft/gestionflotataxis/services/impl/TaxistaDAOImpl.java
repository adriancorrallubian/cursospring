package com.aclsoft.gestionflotataxis.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.aclsoft.gestionflotataxis.model.Taxi;
import com.aclsoft.gestionflotataxis.model.Taxista;
import com.aclsoft.gestionflotataxis.repository.Repository;
import com.aclsoft.gestionflotataxis.services.TaxistaDAO;

@Component
public class TaxistaDAOImpl implements TaxistaDAO{

	@Override
	public Taxista getById(int id) {
		Map<Integer,Taxista> taxista = Repository.getTaxistas();
		
		return taxista.get(id);
	}

	@Override
	public void save(Taxista taxista) {
		
		Map<Integer,Taxista> taxistas = Repository.getTaxistas();
		taxistas.put(taxista.getId(), taxista);
		
	}

	@Override
	public List<Taxista> getAll() {

//		Map<Integer, Taxista> taxistas = Repository.getTaxistas();
//		
//		List<Taxista> taxistaList = new ArrayList<>();
//		
//		for (Entry<Integer, Taxista> taxist : taxistas.entrySet()){
//			Taxista taxi =  taxist.getValue();
//			if (taxi != null){
//				taxistaList.add(taxi);
//			}
//		}
//			
		return new ArrayList<Taxista>(Repository.getTaxistas().values());
	}

}
