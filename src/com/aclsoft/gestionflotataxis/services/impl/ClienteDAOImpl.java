package com.aclsoft.gestionflotataxis.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.aclsoft.gestionflotataxis.model.Cliente;
import com.aclsoft.gestionflotataxis.repository.Repository;
import com.aclsoft.gestionflotataxis.services.ClienteDAO;

@Component
public class ClienteDAOImpl implements ClienteDAO{

	@Override
	public Cliente getById(int id) {
		Map<Integer,Cliente> clientes = Repository.getClientes();
		
		return clientes.get(id);
	}

	@Override
	public void save(Cliente cliente) {
		Map<Integer,Cliente> clientes = Repository.getClientes();
		clientes.put(cliente.getId(), cliente);		
	}

	@Override
	public List<Cliente> getAll() {
//		Map<Integer,Cliente> clientes = Repository.getClientes();
//		
//		List<Cliente> clienteList = new ArrayList<>();
//		
//		for (Entry<Integer, Cliente> client : clientes.entrySet()){
//			Cliente cliente = client.getValue();
//			if (cliente != null){
//				clienteList.add(cliente);
//			}
//		}
		
		return new ArrayList<Cliente>(Repository.getClientes().values());
	}

}
