package com.aclsoft.gestionflotataxis.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.aclsoft.gestionflotataxis.model.Cliente;
import com.aclsoft.gestionflotataxis.model.Taxi;
import com.aclsoft.gestionflotataxis.repository.Repository;
import com.aclsoft.gestionflotataxis.services.TaxiDAO;

@Component
public class TaxiDAOImpl implements TaxiDAO{

	@Override
	public Taxi getByMatricula(String matricula) {
		Map<String,Taxi> taxis = Repository.getTaxis();
		
		return taxis.get(matricula);
	}

	@Override
	public void save(Taxi taxi) {
		Map<String,Taxi> taxis = Repository.getTaxis();
		taxis.put(taxi.getMatricula(), taxi);
	}

	@Override
	public List<Taxi> getAll() {
//		Map<String,Taxi> taxis = Repository.getTaxis();
//		
//		List<Taxi> taxisList = new ArrayList<>();
//		
//		for (Entry<String, Taxi> entry : taxis.entrySet()){
//			Taxi taxi = (Taxi) entry.getValue();
//			if (taxi != null){
//				taxisList.add(taxi);
//			}
//		}
		
		return new ArrayList<Taxi>(Repository.getTaxis().values());
	}

}
