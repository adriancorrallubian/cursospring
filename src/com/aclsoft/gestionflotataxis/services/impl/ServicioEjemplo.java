package com.aclsoft.gestionflotataxis.services.impl;

public class ServicioEjemplo {
	
	private String user;
	private String IP;
	
	public void setUser(String user){
		this.user = user;
	}

	
	public void setIP(String IP){
		this.IP = IP;
	}
	
	
	//Vamos a intentar interceptar este metodo
	public void mostrar (){
		System.out.println(this.user + ": " + this.IP);
		
	}
}
