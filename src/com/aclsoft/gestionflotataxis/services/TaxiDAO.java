package com.aclsoft.gestionflotataxis.services;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.aclsoft.gestionflotataxis.model.Taxi;

public interface TaxiDAO {

	public Taxi getByMatricula(String matricula); //READ
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void save(Taxi taxi); //CREATE
	public List<Taxi> getAll(); //OBTENCION DE LISTAS
	
}
