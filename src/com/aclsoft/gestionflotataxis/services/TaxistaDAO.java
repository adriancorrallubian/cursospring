package com.aclsoft.gestionflotataxis.services;

import java.util.List;

import org.springframework.security.access.prepost.PreAuthorize;

import com.aclsoft.gestionflotataxis.model.Taxista;

public interface TaxistaDAO {

	public Taxista getById(int id);
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void save(Taxista taxista);
	public List<Taxista> getAll();
}
