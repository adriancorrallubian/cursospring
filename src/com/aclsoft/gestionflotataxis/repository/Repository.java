package com.aclsoft.gestionflotataxis.repository;

import java.util.HashMap;
import java.util.Map;

import com.aclsoft.gestionflotataxis.model.Cliente;
import com.aclsoft.gestionflotataxis.model.Taxi;
import com.aclsoft.gestionflotataxis.model.Taxista;

public class Repository {

	private static final Map<String,Taxi> TAXIS = new HashMap<>();
	private static final Map<Integer,Cliente> CLIENTES = new HashMap<>();
	private static final Map<Integer, Taxista> TAXISTAS = new HashMap<>();
	
	
	static{
		
		Taxi t1 = new Taxi("1234BBB", "SEAT", "IBIZA");
		Taxi t2 = new Taxi("4321CCC", "FORD", "MUSTANG");
		Taxi t3 = new Taxi("5678DDD", "PORSCHE", "CARRERA");
		Taxi t4 = new Taxi("8765EEE", "LAMBORGINI", "MURCIELAGO");
		
		Taxista taxista1 = new Taxista (1, "Pep�n","Galvez", "Ridruejo", "48940745B");
		Taxista taxista2 = new Taxista (2, "Adrian","Corral", "Lubian", "12345678C");
		
		Cliente cliente1 = new Cliente (100,"Antonio", "Herrero", "Bayo", true);
		Cliente cliente2 = new Cliente (200, "Manuel", "Leanez", "Ponce", false);
		Cliente cliente3 = new Cliente (300,"Manuel", "Gallardo", "Horta", true);
		
		TAXIS.put(t1.getMatricula(), t1);
		TAXIS.put(t2.getMatricula(), t2);
		TAXIS.put(t3.getMatricula(), t3);
		
		TAXISTAS.put(taxista1.getId(), taxista1);
		TAXISTAS.put(taxista2.getId(), taxista2);
		
		CLIENTES.put(cliente1.getId(), cliente1);
		CLIENTES.put(cliente2.getId(), cliente2);
		CLIENTES.put(cliente3.getId(), cliente3);
		
		
	}
	private Repository(){
		
	}
	
	//Posteriormente todo esto se configurara con Spring
	
	//Metodos estaticos accesores a las colecciones
	public static Map<String,Taxi> getTaxis(){
		return TAXIS;
	}
	
	public static Map<Integer,Taxista> getTaxistas(){
		return TAXISTAS;
	}
	
	public static Map<Integer,Cliente> getClientes(){
		return CLIENTES;
	}
}
