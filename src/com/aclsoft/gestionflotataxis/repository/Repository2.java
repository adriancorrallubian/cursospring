package com.aclsoft.gestionflotataxis.repository;

import java.util.List;
import java.util.Map;

import com.aclsoft.gestionflotataxis.model.Carrera;
import com.aclsoft.gestionflotataxis.model.Cliente;
import com.aclsoft.gestionflotataxis.model.Taxi;
import com.aclsoft.gestionflotataxis.model.Taxista;

public class Repository2 {

	private List<Taxi> taxis;
	private Map<Integer,Cliente> clientes;
	private Map<Integer,Taxista> taxistas;
	private Map<Integer,Carrera> carreras;

	private Repository2(){
		System.out.println("");
	}

	public List<Taxi> getTaxis() {
		return taxis;
	}

	public void setTaxis(List<Taxi> taxis) {
		this.taxis = taxis;
	}

	public Map<Integer, Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(Map<Integer, Cliente> clientes) {
		this.clientes = clientes;
	}

	public Map<Integer, Taxista> getTaxistas() {
		return taxistas;
	}

	public void setTaxistas(Map<Integer, Taxista> taxistas) {
		this.taxistas = taxistas;
	}

	public Map<Integer, Carrera> getCarreras() {
		return carreras;
	}

	public void setCarreras(Map<Integer, Carrera> carreras) {
		this.carreras = carreras;
	}

	
	
	
	
}
