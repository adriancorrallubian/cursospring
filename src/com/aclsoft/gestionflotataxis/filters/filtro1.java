package com.aclsoft.gestionflotataxis.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter("/filtro1")
public class filtro1 implements Filter {

//ANIADIMOS UN FILTRO

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
// ENTRADA!!
		//Zona para procesar el request...
		System.out.println("Filtro1 acaba de interceptar un request");
		
		chain.doFilter(request, response);
		
		
		System.out.println("Filtro1 actuando sobre el response");
	}

	
	
	
	
	
	
	
	
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
	
	public void destroy() {
		// TODO Auto-generated method stub
	}


}
