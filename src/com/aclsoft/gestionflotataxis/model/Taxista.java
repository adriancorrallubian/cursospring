package com.aclsoft.gestionflotataxis.model;

public class Taxista extends Persona{

	private String licencia;
	
	public Taxista(){
		
	}

	public Taxista(int id, String nombre, String apellido1, String apellido2, String licencia){
		super(id, nombre, apellido1, apellido2);
		this.licencia = licencia;
	}
	
	public String getLicencia() {
		return licencia;
	}



	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}

	@Override
	public String toString() {
		return "Taxista [licencia=" + licencia + ", getId()=" + getId() + ", getNombre()=" + getNombre()
				+ ", getApellido1()=" + getApellido1() + ", getApellido2()=" + getApellido2() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
}
