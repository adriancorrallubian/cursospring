package com.aclsoft.gestionflotataxis.model;

import com.sun.org.glassfish.gmbal.DescriptorKey;

public abstract class Persona {
	
	@DescriptorKey(value = "id")
	private int id;
	private String nombre;
	private String apellido1;
	private String apellido2;
	
	public Persona(){
		
	}

	public Persona(int id, String nombre, String apellido1, String apellido2) {
		this.id = id;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	@Override
	public String toString() {
		return "Persona [id=" + id + ", nombre=" + nombre + ", apellido1=" + apellido1 + ", apellido2=" + apellido2
				+ "]";
	}

}
