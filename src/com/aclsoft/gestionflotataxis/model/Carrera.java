package com.aclsoft.gestionflotataxis.model;

public class Carrera {
	
	private int id;
	private Taxista taxista;
	private Taxi taxi;
	private Cliente cliente;
	
	private String origen;
	private String destino;
	
	
	public Carrera(){
		
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Taxista getTaxista() {
		return taxista;
	}


	public void setTaxista(Taxista taxista) {
		this.taxista = taxista;
	}


	public Taxi getTaxi() {
		return taxi;
	}


	public void setTaxi(Taxi taxi) {
		this.taxi = taxi;
	}


	public Cliente getCliente() {
		return cliente;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


	public String getOrigen() {
		return origen;
	}


	public void setOrigen(String origen) {
		this.origen = origen;
	}


	public String getDestino() {
		return destino;
	}


	public void setDestino(String destino) {
		this.destino = destino;
	}


	@Override
	public String toString() {
		return "Carrera [id=" + id + ", taxista=" + taxista + ", taxi=" + taxi + ", cliente=" + cliente + ", origen="
				+ origen + ", destino=" + destino + "]";
	}

}
