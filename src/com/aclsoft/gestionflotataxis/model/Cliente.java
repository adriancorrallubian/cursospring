package com.aclsoft.gestionflotataxis.model;

public class Cliente extends Persona{
	
	private boolean tarjetaGold;
	
	public Cliente(){
		
	}
	
	public Cliente(int id, String nombre, String apellido1, String apellido2, Boolean tarjetaGold){
		super(id, nombre, apellido1, apellido2);
		this.tarjetaGold = tarjetaGold;
	}
	
	public boolean isTarjetaGold() {
		return tarjetaGold;
	}
	public void setTarjetaGold(boolean tarjetaGold) {
		this.tarjetaGold = tarjetaGold;
	}

	@Override
	public String toString() {
		return "Cliente [tarjetaGold=" + tarjetaGold + ", getId()=" + getId() + ", getNombre()=" + getNombre()
				+ ", getApellido1()=" + getApellido1() + ", getApellido2()=" + getApellido2() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}


}
